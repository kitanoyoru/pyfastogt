import os
import distro
import platform
import subprocess

from abc import ABCMeta, abstractmethod
from typing import Dict, List, Union


class OSTypes:
    WINDOWS = "windows"
    LINUX = "linux"
    MAC_OSX = "macosx"
    FREE_BSD = "freebsd"
    ANDROID = "android"

    UNKNOWN = "unknown"


class DistroTypes:
    RHEL = "RHEL"
    DEBIAN = "DEBIAN"
    ARCH = "ARCH"
    UBUNTU = "UBUNTU"


class ArchTypes:
    ARMV6L = "armv6l"
    ARMV7L = "armv7l"
    I386 = "i386"
    I686 = "i686"
    X86_64 = "x86_64"
    AMD64 = "amd64"
    AARCH64 = "aarch64"


class ExtensionTypes:
    DEB = "deb"
    RPM = "rpm"
    TGZ = "tar.gz"
    EXE = "exe"
    ZIP = "zip"
    DMG = "dmg"
    APK = "apk"

    UNKNOWN = "unknown"


class Architecture:
    def __init__(self, arch: str, bit: int, default_install_prefix_path: str) -> None:
        self.name_ = arch
        self.bit_ = bit
        self.default_install_prefix_path_ = default_install_prefix_path

    def name(self) -> str:
        return self.name_

    def bit(self) -> int:
        return self.bit_

    def default_install_prefix_path(self) -> str:
        return self.default_install_prefix_path_


class Platform(metaclass=ABCMeta):
    def __init__(
        self, name: OSTypes, architecture: Architecture, package_types: List[str]
    ) -> None:
        self.name_ = name
        self.architecture_ = architecture
        self.package_types_ = package_types

    def name(self) -> OSTypes:
        return self.name_

    def architecture(self) -> Architecture:
        return self.architecture_

    def package_types(self) -> list:
        return self.package_types_

    @abstractmethod
    def install_package(self, name: str):
        pass

    @abstractmethod
    def get_human_readable_name(self) -> str:
        pass

    def env_variables(self) -> dict:
        return {}

    def cmake_specific_flags(self) -> List:
        return []

    def configure_specific_flags(self) -> List:
        return []


class SupportedPlatforms(metaclass=ABCMeta):
    def __init__(
        self, name: OSTypes, architectures: List[Architecture], package_types: List[str]
    ) -> None:
        self.name_ = name
        self.architectures_ = architectures
        self.package_types_ = package_types

    def name(self) -> OSTypes:
        return self.name_

    def architectures(self) -> List[Architecture]:
        return self.architectures_

    def package_types(self) -> List:
        return self.package_types_

    def get_architecture_by_arch_name(self, name: str) -> Architecture:
        return next((x for x in self.architectures_ if x.name() == name), None)

    @abstractmethod
    def make_platform_by_arch(
        self, arch: Architecture, package_types: List[str]
    ) -> Platform:  # factory method
        pass


def linux_get_dist() -> Union[DistroTypes, NotImplementedError]:
    """
    Return the running distribution group
    RHEL: RHEL, CENTOS, FEDORA
    DEBIAN: UBUNTU, DEBIAN, LINUXMINT
    """
    linux_tuple = distro.linux_distribution()
    dist_name = linux_tuple[0]
    dist_name_upper = dist_name.upper()

    if dist_name_upper.startswith(("RHEL", "CENTOS LINUX", "FEDORA", "AMAZON LINUX")):
        return DistroTypes.RHEL
    elif dist_name_upper.startswith(
        ("DEBIAN", "UBUNTU", "LINUX MINT", "RASPBIAN GNU/LINUX")
    ):
        return DistroTypes.DEBIAN
    elif dist_name_upper.startswith(("ARCH")):
        return DistroTypes.ARCH

    raise NotImplementedError("Unknown platform '%s'" % dist_name)


# Linux platforms


class DebianPlatform(Platform):
    def __init__(self, arch: Architecture, package_types: List[str]) -> None:
        Platform.__init__(self, OSTypes.LINUX, arch, package_types)

    def install_package(self, name: str) -> None:
        subprocess.call(
            ["apt-get", "-y", "--no-install-recommends", "install", name])

    def get_human_readable_name(self) -> str:
        return "Debian"


class RedHatPlatform(Platform):
    def __init__(self, arch: Architecture, package_types: List[str]) -> None:
        Platform.__init__(self, OSTypes.LINUX, arch, package_types)

    def install_package(self, name: str):
        subprocess.call(["yum", "-y", "install", name])

    def get_human_readable_name(self) -> str:
        return "Red Hat"


class ArchPlatform(Platform):
    def __init__(self, arch: Architecture, package_types: List[str]) -> None:
        Platform.__init__(self, OSTypes.LINUX, arch, package_types)

    def install_package(self, name: str):
        subprocess.call(["pacman", "-S", "--noconfirm", name])

    def get_human_readable_name(self) -> str:
        return "Arch Linux"


class UbuntuPlatform(DebianPlatform):
    def get_human_readable_name(self) -> str:
        return "Ubuntu"


class RaspberryPiPlatform(DebianPlatform):
    def get_human_readable_name(self) -> str:
        return "Raspberry Pi"


class LinuxPlatforms(SupportedPlatforms):
    def __init__(self):
        SupportedPlatforms.__init__(
            self,
            OSTypes.LINUX,
            [
                Architecture(ArchTypes.X86_64, 64, "/usr/local"),
                Architecture(ArchTypes.I386, 32, "/usr/local"),
                Architecture(ArchTypes.I686, 32, "/usr/local"),
                Architecture(ArchTypes.AARCH64, 64, "/usr/local"),
                Architecture(ArchTypes.ARMV7L, 32, "/usr/local"),
                Architecture(ArchTypes.ARMV6L, 32, "/usr/local"),
            ],
            ["DEB", "RPM", "TGZ"],
        )

    def make_platform_by_arch(
        self, arch: Architecture, package_types: List[str]
    ) -> Union[Platform, NotImplementedError]:
        distr = linux_get_dist()

        if distr == DistroTypes.DEBIAN:
            return DebianPlatform(arch, package_types)
        elif distr == DistroTypes.RHEL:
            return RedHatPlatform(arch, package_types)
        elif distr == DistroTypes.ARCH:
            return ArchPlatform(arch, package_types)
        elif distr == DistroTypes.UBUNTU:
            return UbuntuPlatform(arch, package_types)

        raise NotImplementedError("Unknown distribution '%s'" % distr)


# Windows platforms
class WindowsMingwPlatform(Platform):
    def __init__(self, arch: Architecture, package_types: List[str]) -> None:
        Platform.__init__(self, OSTypes.WINDOWS, arch, package_types)

    def install_package(self, name: str) -> None:
        subprocess.call(["pacman", "-S", "--noconfirm", name])

    def get_human_readable_name(self) -> str:
        return "Windows"


class WindowsPlatforms(SupportedPlatforms):
    def __init__(self):
        SupportedPlatforms.__init__(
            self,
            OSTypes.WINDOWS,
            [
                Architecture(ArchTypes.X86_64, 64, "/mingw64"),
                Architecture(ArchTypes.AMD64, 64, "/mingw64"),
                Architecture(ArchTypes.I386, 32, "/mingw32"),
                Architecture(ArchTypes.I686, 32, "/mingw32"),
            ],
            ["NSIS", "ZIP"],
        )

    def make_platform_by_arch(
        self, arch: Architecture, package_types: list
    ) -> Platform:
        return WindowsMingwPlatform(arch, package_types)


# MacOSX platforms
class MacOSXCommonPlatform(Platform):
    def __init__(self, arch: Architecture, package_types: List[str]) -> None:
        Platform.__init__(self, OSTypes.MAC_OSX, arch, package_types)

    def install_package(self, name: str):
        subprocess.call(["port", "-N", "install", name])

    def get_human_readable_name(self) -> str:
        return "MacOSX"


class MacOSXPlatforms(SupportedPlatforms):
    def __init__(self):
        SupportedPlatforms.__init__(
            self,
            OSTypes.MAC_OSX,
            [Architecture(ArchTypes.X86_64, 64, "/usr/local")],
            ["DragNDrop", "ZIP"],
        )

    def make_platform_by_arch(
        self, arch: Architecture, package_types: List[str]
    ) -> Platform:
        return MacOSXCommonPlatform(arch, package_types)


# FreeBSD platforms
class FreeBSDCommonPlatform(Platform):
    def __init__(self, arch: Architecture, package_types: list):
        Platform.__init__(self, OSTypes.FREE_BSD, arch, package_types)

    def install_package(self, name: str):
        subprocess.call(["pkg", "install", "-y", name])

    def get_human_readable_name(self) -> str:
        return "FreeBSD"


class FreeBSDPlatforms(SupportedPlatforms):
    def __init__(self):
        SupportedPlatforms.__init__(
            self,
            OSTypes.FREE_BSD,
            [
                Architecture(ArchTypes.X86_64, 64, "/usr/local"),
                Architecture(ArchTypes.AMD64, 64, "/usr/local"),
            ],
            ["TGZ"],
        )

    def make_platform_by_arch(
        self, arch: Architecture, package_types: List[str]
    ) -> Platform:
        return FreeBSDCommonPlatform(arch, package_types)


# Android platforms
ANDROID_PLATFORM_NUMBER = 16
ANDROID_PLATFORM = "android-%s" % ANDROID_PLATFORM_NUMBER
ANDROID_NDK = "~/Android/Sdk/ndk-bundle"


class AndroidCommonPlatform(Platform):
    def __init__(self, arch: Architecture, package_types: List[str]) -> None:
        Platform.__init__(self, OSTypes.ANDROID, arch, package_types)

    def install_package(self, name: str) -> NotImplementedError:
        raise NotImplementedError(
            "You need to define a install_package method!")

    def get_human_readable_name(self) -> str:
        return "Android"

    def env_variables(self) -> Dict[str, str]:
        arch = self.architecture()
        abs_prefix_path = os.path.expanduser(ANDROID_NDK)
        return {
            "CC": "{0}/toolchains/llvm/prebuilt/linux-x86_64/bin/{1}-linux-androideabi{2}-clang".format(
                abs_prefix_path, arch.name(), ANDROID_PLATFORM_NUMBER
            ),
            "CXX": "{0}/toolchains/llvm/prebuilt/linux-x86_64/bin/{1}-linux-androideabi{2}-clang++".format(
                abs_prefix_path, arch.name(), ANDROID_PLATFORM_NUMBER
            ),
        }

    def cmake_specific_flags(self) -> List[str]:
        abs_prefix_path = os.path.expanduser(ANDROID_NDK)
        return [
            "-DCMAKE_TOOLCHAIN_FILE=%s/build/cmake/android.toolchain.cmake"
            % abs_prefix_path,
            "-DANDROID_PLATFORM=%s" % ANDROID_PLATFORM,
        ]

    def configure_specific_flags(self) -> List[str]:
        arch = self.architecture()
        return ["--host=%s-linux-androideabi" % arch.name()]


class AndroidPlatforms(SupportedPlatforms):
    def __init__(self):
        SupportedPlatforms.__init__(
            self,
            OSTypes.ANDROID,
            [
                Architecture(
                    ArchTypes.ARMV7L,
                    32,
                    ANDROID_NDK + "/platforms/" + ANDROID_PLATFORM + "/arch-arm/usr/",
                ),
                Architecture(
                    ArchTypes.I686,
                    32,
                    ANDROID_NDK + "/platforms/" + ANDROID_PLATFORM + "/arch-x86/usr/",
                ),
                Architecture(
                    ArchTypes.X86_64,
                    64,
                    ANDROID_NDK + "/platforms/" + ANDROID_PLATFORM + "/arch-x86/usr/",
                ),
                Architecture(
                    ArchTypes.AARCH64,
                    64,
                    ANDROID_NDK + "/platforms/" + ANDROID_PLATFORM + "/arch-x86/usr/",
                ),
            ],
            ["APK"],
        )

    def make_platform_by_arch(
        self, arch: Architecture, package_types: List[str]
    ) -> Platform:
        return AndroidCommonPlatform(arch, package_types)


SUPPORTED_PLATFORMS = [
    LinuxPlatforms(),
    WindowsPlatforms(),
    MacOSXPlatforms(),
    FreeBSDPlatforms(),
    AndroidPlatforms(),
]


def get_extension_by_package(package_type: str) -> ExtensionTypes:
    if package_type == "DEB":
        return ExtensionTypes.DEB
    elif package_type == "RPM":
        return ExtensionTypes.RPM
    elif package_type == "TGZ":
        return ExtensionTypes.TGZ
    elif package_type == "NSIS":
        return ExtensionTypes.EXE
    elif package_type == "ZIP":
        return ExtensionTypes.ZIP
    elif package_type == "DragNDrop":
        return ExtensionTypes.DMG
    elif package_type == "APK":
        return ExtensionTypes.APK

    return ExtensionTypes.UNKNOWN


def get_os() -> OSTypes:
    uname_str = platform.system()

    if "MINGW" in uname_str:
        return OSTypes.WINDOWS
    elif "MSYS" in uname_str:
        return OSTypes.WINDOWS
    elif uname_str == "Windows":
        return OSTypes.WINDOWS
    elif uname_str == "Linux":
        return OSTypes.LINUX
    elif uname_str == "Darwin":
        return OSTypes.MAC_OSX
    elif uname_str == "FreeBSD":
        return OSTypes.FREE_BSD
    elif uname_str == "Android":
        return OSTypes.ANDROID

    return OSTypes.UNKNOWN


def get_arch_name() -> str:
    return platform.machine()


def get_supported_platform_by_name(name: str) -> SupportedPlatforms:
    return next((x for x in SUPPORTED_PLATFORMS if x.name() == name), None)


def stable_path(path: str) -> str:
    if get_os() == OSTypes.WINDOWS:
        return path.replace("\\", "/")

    return path.replace("\\", "/")
